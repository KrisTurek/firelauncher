# FireLauncher #

FireLauncher is a port of the AppStarter project for Amazon devices.
See https://github.com/sphinx02/AppStarter for the original project.

## System requirements

* [Android](https://www.android.com/) (minimum API Level 19 Android 4.4 KitKat)
