package com.firelauncher.errors.exceptions;

public class PolskaTvException extends Exception {

    private ExceptionModel exception;

    public PolskaTvException() {
        super();
    }

    public PolskaTvException(ExceptionModel exception) {
        super("Error message=[" + exception.getMessage() + "] code=[" + exception.getCode() + "]");
        this.exception = exception;
    }

    public PolskaTvException(int code, String message) {
        super("Error message=[" + message + "] code=[" + code + "]");
    }

    public PolskaTvException(String message) {
        super(message);
    }

    public PolskaTvException(Throwable cause) {
        super(cause);
    }

    public PolskaTvException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionModel getException() {
        return exception;
    }

    public void setException(ExceptionModel exception) {
        this.exception = exception;
    }

}