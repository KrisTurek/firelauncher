package com.firelauncher.errors;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.firelauncher.activities.ForceCloseActivity;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.utils.DiagnosticUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {

    private final Activity context;

    public ExceptionHandler(Activity context) {
        this.context = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        Log.e(DebugTag.APP, exception.getMessage(), exception);

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = DiagnosticUtil.deviceInformation(context);
        errorReport.append("************ CAUSE OF ERROR ************\n");
        errorReport.append(stackTrace.toString());

        Intent intent = new Intent(context, ForceCloseActivity.class);
        intent.putExtra("error", errorReport.toString());
        context.startActivity(intent);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }

}