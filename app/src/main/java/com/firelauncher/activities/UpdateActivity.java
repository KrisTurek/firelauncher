package com.firelauncher.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.firelauncher.errors.ExceptionHandler;
import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.services.UpdateService;
import com.firelauncher.utils.PrefUtil;
import com.firelauncher.utils.Version;

import java.io.File;

public class UpdateActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.onCreate() - begin");

        if (isAvailableUpdate()) {
            installUpdate();
            shutdown();
        } else {
            startUpdaterService();
            startMainActivity();
        }

        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.onCreate() - end");
    }

    private void shutdown() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    private void startMainActivity() {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.startMainActivity()");

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void startUpdaterService() {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.startUpdaterService()");

        Intent serviceIntent = new Intent(this, UpdateService.class);
        startService(serviceIntent);
    }

    private void installUpdate() {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.installUpdate()");

        String path = PrefUtil.getFromPrefs(this, PrefUtil.PREFS_UPDATE_PATH_KEY, "");

        if (path != null && !path.equals("")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private boolean isAvailableUpdate() {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateActivity.isAvailableUpdate()");
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            Version currentVersion = new Version(pInfo.versionName);
            Version newVersion = new Version(PrefUtil.getFromPrefs(this, PrefUtil.PREFS_UPDATE_VER_KEY, "0"));

            if (newVersion.compareTo(currentVersion) > 0)
                return true;
            else
                return false;

        } catch (PackageManager.NameNotFoundException ex) {
            Log.e(DebugTag.APP, ex.getMessage(), ex);
        }
        return false;
    }
}
