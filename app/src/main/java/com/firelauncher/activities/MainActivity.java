package com.firelauncher.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.firelauncher.R;
import com.firelauncher.adapters.MenuItemsListAdapter;
import com.firelauncher.errors.ExceptionHandler;
import com.firelauncher.fragments.AppsFragment;
import com.firelauncher.fragments.commons.CustomFragment;
import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.services.UpdateService;
import com.firelauncher.utils.CustomResultReceiver;
import com.firelauncher.utils.PrefUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity implements CustomResultReceiver.Receiver {

    private LinearLayout mMainLayout;
    private LinearLayout mSemiTransparentLayout;
    private ListView mListView;
    private Fragment mLastSetFragment;

    private TextView mTextViewClock;
    private TextView mTextViewDate;

    private Timer mTimer;

    private CustomResultReceiver mReceiver;
    private AlarmManager mAlarmManager;
    private PendingIntent mPendingIntent;

    @Override
    protected void onDestroy() {
        mAlarmManager.cancel(mPendingIntent);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        setContentView(R.layout.main_activity);

        mMainLayout = (LinearLayout) findViewById(R.id.main_linear_layout);
        mMainLayout.setBackground(getBitmapFromAsset("wallpaper.jpg"));

        mSemiTransparentLayout = (LinearLayout) findViewById(R.id.linearLayoutSemiTransparentBackground);
        mSemiTransparentLayout.setBackgroundResource(R.color.semitransparentbackground_medium);

        mTextViewClock = (TextView) findViewById(R.id.textViewClock);
        mTextViewDate = (TextView) findViewById(R.id.textViewDate);

        mListView = (ListView) findViewById(R.id.listView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleMenuItemSelection(parent, view, position, id);
            }
        });

        mListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                handleMenuItemSelection(parent, view, position, id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        MenuItemsListAdapter adapter = new MenuItemsListAdapter(this);
        mListView.setAdapter(adapter);

        mListView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                try {
                    mListView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    mListView.requestFocusFromTouch();
                    mListView.setSelection(0);
                } catch (Exception e) {
                    Log.e(DebugTag.APP, e.getMessage(), e.getCause());
                }
            }
        });
    }

    public BitmapDrawable getBitmapFromAsset(String filePath) {
        AssetManager assetManager = getAssets();

        try {
            InputStream istr = assetManager.open(filePath);
            Bitmap bitmap = BitmapFactory.decodeStream(istr);

            BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
            return drawable;
        } catch (IOException e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
        return null;
    }

    @Override
    public void onResume() {

        mReceiver = new CustomResultReceiver(new Handler());
        mReceiver.setReceiver(this);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(this, UpdateService.class);
        notificationIntent.putExtra("CustomResultReceiver", mReceiver);
        mPendingIntent = PendingIntent.getService(this, 0, notificationIntent, 0);
        mAlarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 1, mPendingIntent);

        setDateAndTime();
        startTimer();

        super.onResume();
    }

    @Override
    public void onPause() {
        stopTimer();

        super.onPause();
    }

    private void setActiveFragment(Fragment fragment) {
        try {
            mLastSetFragment = fragment;

            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.item_detail_container, fragment);
            fragmentTransaction.commit();
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
    }

    private void handleMenuItemSelection(AdapterView<?> parent, View view, int position, long id) {
        try {
            Fragment fragment = (Fragment) Class.forName(((MenuItemsListAdapter) parent.getAdapter()).getItem(position).className).getConstructor().newInstance();

            setActiveFragment(fragment);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
    }

    private void startTimer() {
        mTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                setDateAndTime();
            }
        };

        Integer everyXminute = 1;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Integer toBeAdded = everyXminute - (calendar.get(Calendar.MINUTE) % everyXminute);
        if (toBeAdded == 0) {
            toBeAdded = everyXminute;
        }
        calendar.add(Calendar.MINUTE, toBeAdded);

        mTimer.schedule(timerTask, calendar.getTime(), 1000 * 60 * everyXminute);
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void setDateAndTime() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date actDateTime = new Date();
                DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, getResources().getConfiguration().locale);
                mTextViewDate.setText(dateFormat.format(actDateTime));
                mTextViewClock.setText(DateUtils.formatDateTime(MainActivity.this, actDateTime.getTime(), DateUtils.FORMAT_SHOW_TIME));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent e) {
        if (mLastSetFragment != null && mLastSetFragment instanceof CustomFragment) {
            Boolean retVal = ((CustomFragment) mLastSetFragment).onKeyDown(keycode, e);
            if (retVal) {
                return true;
            }
        }

        return super.onKeyDown(keycode, e);
    }

    @Override
    public void onBackPressed() {
        boolean isHandled = false;

        if (mLastSetFragment != null && mLastSetFragment instanceof CustomFragment) {
            CustomFragment actFragment = (CustomFragment) mLastSetFragment;
            isHandled = ((CustomFragment) mLastSetFragment).onBackPressed();
        }

        if (!isHandled) {
            if (!(mLastSetFragment instanceof AppsFragment)) {
                mListView.setSelection(0);
            }
        }
    }

    private void startUpdateService() {
        Intent i = new Intent(this, UpdateService.class);
        i.putExtra("CustomResultReceiver", mReceiver);
        startService(i);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "MainActivity.onReceiveResult()[" + resultCode + "][" + resultData + "]");

        if (resultCode == 1) {
            installUpdate();
        }
    }

    private void installUpdate() {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "MainActivity.installUpdate()");

        String path = PrefUtil.getFromPrefs(this, PrefUtil.PREFS_UPDATE_PATH_KEY, "");

        if (path != null && !path.equals("")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
