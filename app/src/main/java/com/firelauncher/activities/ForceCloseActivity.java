package com.firelauncher.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.TextView;

import com.firelauncher.R;
import com.firelauncher.errors.ExceptionHandler;
import com.firelauncher.tasks.UploadLogsAsyncTask;

//TODO dodac w MainActivity, AppsFragment, SettingsFragment wywolanie
public class ForceCloseActivity extends Activity {

    private TextView error;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.force_close_activity);

        error = (TextView) findViewById(R.id.error);
        error.setText(getIntent().getStringExtra("error"));

        UploadLogsAsyncTask task = new UploadLogsAsyncTask(getApplicationContext());
        task.execute(getIntent().getStringExtra("error"));
    }
}
