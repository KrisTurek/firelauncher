package com.firelauncher.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.firelauncher.logging.DebugTag;

public class PrefUtil {

    public static final String PREFS_UPDATE_VER_KEY = "__UPDATE_VER__";
    public static final String PREFS_UPDATE_PATH_KEY = "__UPDATE_PATH__";
    public static final String PREFS_SOFTWARE_KEY = "__SOFTWARE__";
    public static final String PREFS_CLEAR_CACHE_KEY = "__CLEAR_CACHE__";
    public static final String PREFS_UPLOAD_LOGS_KEY = "__UPLOAD_LOGS__";
    public static final String PREFS_WIFI_NAME_KEY = "__WIFI_NAME__";
    public static final String PREFS_IP_ADDRESS_KEY = "__IP_ADDRESS__";
    public static final String PREFS_PREVENT_HOME_KEY = "__PREVENT_HOME__";
    public static final String PREFS_SHOW_SYS_APPS_KEY = "__SHOW_SYS_APPS__";
    public static final String PREFS_HIDDEN_APPS_KEY = "__HIDDEN_APPS__";
    public static final String PREFS_DEBUG_KEY = "__DEBUG__";

    public static final boolean DEFAULT_PREVENT_HOME = true;
    public static final boolean DEFAULT_SHOW_SYS_APPS = false;
    public static final boolean DEFAULT_DEBUG = true;

    public static void saveToPrefs(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveToPrefs(Context context, String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void saveToPrefs(Context context, String key, Integer value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void clearPrefs(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    public static void clearPrefs(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();

        editor.remove(key);
        editor.commit();
    }

    public static boolean contains(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.contains(key);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
            return false;
        }
    }

    public static String getFromPrefs(Context context, String key, String defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getString(key, defaultValue);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
            return defaultValue;
        }
    }

    public static boolean getFromPrefs(Context context, String key, boolean defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getBoolean(key, defaultValue);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
            return defaultValue;
        }
    }

    public static int getFromPrefs(Context context, String key, int defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getInt(key, defaultValue);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
            return defaultValue;
        }
    }
}
