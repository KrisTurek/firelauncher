package com.firelauncher.utils;

import android.content.Context;

import java.io.File;

public class CacheUtil {

    public static void deleteCache(Context context) {
        File dir = context.getExternalCacheDir();
        deleteDir(dir);

        dir = context.getCacheDir();
        deleteDir(dir);
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
