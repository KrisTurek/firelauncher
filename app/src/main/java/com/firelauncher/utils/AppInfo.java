package com.firelauncher.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

public class AppInfo extends ApplicationInfo {
    Context mContext;

    public AppInfo(Context context, ApplicationInfo app) {
        super(app);
        mContext = context;
    }

    public String getDisplayName() {
        String retVal = this.loadLabel(mContext.getPackageManager()).toString();
        if (retVal == null || retVal.equals("")) {
            retVal = packageName;
        }
        return retVal;
    }

    public Drawable getDisplayIcon() {
        return this.loadIcon(mContext.getPackageManager());
    }
}
