package com.firelauncher.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DownloadErrorException;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderContinueErrorException;
import com.dropbox.core.v2.files.ListFolderErrorException;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.dropbox.core.v2.files.WriteMode;
import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DropboxUtil {

    private Context context;
    private DbxClientV2 client;

    private static final String ACCESS_TOKEN = "KdfUPQLEbqAAAAAAAAAACuiAOtHvSNJ-y9ogEY_kpPIw-RjHKpKEd9YkurdE1LrU";

    private static final String LOGS_DIR = "firelauncher/logs";
    private static final String UPDATES_DIR = "firelauncher/apks";

    public DropboxUtil(Context context) {
        this.context = context;
    }

    private void login() {
        DbxRequestConfig config = new DbxRequestConfig("FireLauncher");
        client = new DbxClientV2(config, ACCESS_TOKEN);
    }

    public Version getVersion(String ver) {
        return new Version(ver.replace("firelauncher-", "").replace(".apk", ""));
    }

    public File downloadUpdate() {
        try {
            login();

            ListFolderResult result = client.files().listFolder(File.separator + UPDATES_DIR);

            List<Metadata> files = result.getEntries();
            Collections.sort(files, new Comparator<Metadata>() {
                @Override
                public int compare(Metadata o1, Metadata o2) {
                    return getVersion(o1.getName()).compareTo(getVersion(o2.getName()));
                }
            });

            if (DebugConfig.DEBUG) {
                for (Metadata metadata : files) {
                    Log.d(DebugTag.APP, metadata.getName());
                }
            }

            if (files.size() > 0) {
                Metadata medataData = files.get(files.size() - 1);

                Version newVersion = getVersion(medataData.getName());
                Version currentVersion = new Version(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);

                File downloadFile = new File(context.getExternalCacheDir() + File.separator + medataData.getName());

                if (newVersion.compareTo(currentVersion) > 0) {
                    OutputStream outputStream = new FileOutputStream(downloadFile);
                    client.files().download(medataData.getPathLower(), null).download(outputStream);

                    return downloadFile;
                }
            }
        } catch (RuntimeException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (DownloadErrorException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (ListFolderContinueErrorException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (ListFolderErrorException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (DbxException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (FileNotFoundException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        }
        return null;
    }

    public void uploadLogs(String msg) {
        try {
            login();
            SimpleDateFormat yyyyMMddhhmmss = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
            String fileName = yyyyMMddhhmmss.format(new Date());

            File logFile = prepareLogFile(fileName);
            File exFile = prepareExceptionFile(fileName, msg);
            File dumpFile = prepareDumpFile(fileName);

            if (logFile != null && exFile != null) {
                uploadFile(client, logFile, File.separator + LOGS_DIR + File.separator + logFile.getName());
                uploadFile(client, exFile, File.separator + LOGS_DIR + File.separator + exFile.getName());
                uploadFile(client, dumpFile, File.separator + LOGS_DIR + File.separator + dumpFile.getName());

                logFile.delete();
                exFile.delete();
                dumpFile.delete();
            }
        } catch (RuntimeException ex) {
            Log.e(DebugTag.APP, ex.getMessage(), ex);
        }
    }

    private File prepareExceptionFile(String fileName, String error) {
        try {
            File tempFile = new File(context.getExternalCacheDir() + File.separator + fileName + "_ex.txt");
            tempFile.createNewFile();

            FileOutputStream fOut = new FileOutputStream(tempFile);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(error);
            osw.flush();
            osw.close();

            return tempFile;
        } catch (IOException ex) {
            Log.e(DebugTag.APP, ex.getMessage(), ex);
            return null;
        }
    }

    private File prepareLogFile(String fileName) {
        try {
            File tempFile = new File(context.getExternalCacheDir() + File.separator + fileName + "_log.txt");
            tempFile.createNewFile();

            Process process = Runtime.getRuntime().exec("logcat -d -v threadtime com.firelauncher.app:D *:S");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuilder log = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
                log.append(System.getProperty("line.separator"));
            }

            FileOutputStream fOut = new FileOutputStream(tempFile);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(log.toString());
            osw.flush();
            osw.close();

            return tempFile;
        } catch (IOException ex) {
            Log.e(DebugTag.APP, ex.getMessage(), ex);
            return null;
        }
    }

    private File prepareDumpFile(String fileName) {
        try {
            File tempFile = new File(context.getExternalCacheDir() + File.separator + fileName + "_dump.txt");
            tempFile.createNewFile();

            Process process = Runtime.getRuntime().exec("logcat -d -v threadtime");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuilder log = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
                log.append(System.getProperty("line.separator"));
            }

            FileOutputStream fOut = new FileOutputStream(tempFile);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(log.toString());
            osw.flush();
            osw.close();

            return tempFile;
        } catch (IOException ex) {
            Log.e(DebugTag.APP, ex.getMessage(), ex);
            return null;
        }
    }

    private void uploadFile(DbxClientV2 dbxClient, File localFile, String dropboxPath) {
        try (InputStream in = new FileInputStream(localFile)) {
            FileMetadata metadata = dbxClient.files().uploadBuilder(dropboxPath)
                    .withMode(WriteMode.OVERWRITE)
                    .withClientModified(new Date(localFile.lastModified()))
                    .uploadAndFinish(in);
        } catch (UploadErrorException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (DbxException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        }
    }
}
