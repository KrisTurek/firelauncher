package com.firelauncher.utils;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

public class DialogUtil {

    public static void toast(final Context context, final int id) {
        Toast.makeText(context, id, Toast.LENGTH_LONG).show();
    }

    public static void toast(final Context context, final String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void toast(final Activity context, final String message) {
        if (context != null)
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            });
    }
}
