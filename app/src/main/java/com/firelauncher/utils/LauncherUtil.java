package com.firelauncher.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;

import com.firelauncher.adapters.InstalledAppsAdapter;

public class LauncherUtil {

    private static String mLauncherPackageName = null;
    private static Thread mStartAndWatchThread = null;
    private static Object mSyncObj = new Object();

    public static void stopWatchThread() {
        synchronized (mSyncObj) {
            if (DebugConfig.DEBUG)
                Log.d(DebugTag.APP, "Stop watch thread");

            try {
                if (mStartAndWatchThread != null && mStartAndWatchThread.isAlive()) {
                    mStartAndWatchThread.interrupt();
                    mStartAndWatchThread.join();
                    mStartAndWatchThread = null;

                    if (DebugConfig.DEBUG)
                        Log.d(DebugTag.APP, "Watchthread stopped");
                } else {
                    if (DebugConfig.DEBUG)
                        Log.d(DebugTag.APP, "Watchthread was not alive, nothing to be done.");
                }
            } catch (Exception e) {
                Log.e(DebugTag.APP, "Exception while stopping watchthread: \n" + e.getMessage());
            }
        }
    }

    public static void startAppByPackageName(final Context context, String packageName, Boolean isClickAction, Boolean isStartupAction, Boolean isClearPreviousInstancesForced) {
        try {
            stopWatchThread();

            synchronized (mSyncObj) {
                if (packageName != null && !packageName.equals("")) {
                    final Intent launchIntent = InstalledAppsAdapter.getLaunchableIntentByPackageName(context, packageName);
                    if (isStartupAction || isClearPreviousInstancesForced) {
                        if (DebugConfig.DEBUG)
                            Log.d(DebugTag.APP, "Using FLAG_ACTIVITY_CLEAR_TASK: isStartupAction=" + isStartupAction.toString() + ", isClearPreviousInstancesForced=" + isClearPreviousInstancesForced.toString());

                        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    }

                    if (DebugConfig.DEBUG)
                        Log.d(DebugTag.APP, "Starting launcher activity of package: " + packageName);

                    context.startActivity(launchIntent);
                }
            }
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
    }

    public static void startSettingsViewByPackageName(Context context, String packageName) {
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.fromParts("package", packageName, null));

            context.startActivity(intent);
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
    }

    public static synchronized String getLauncherPackageName(Context context) {
        if (mLauncherPackageName == null) {
            final Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);

            PackageManager pm = context.getPackageManager();
            ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
            mLauncherPackageName = resolveInfo.activityInfo.packageName;
        }

        return mLauncherPackageName;
    }
}
