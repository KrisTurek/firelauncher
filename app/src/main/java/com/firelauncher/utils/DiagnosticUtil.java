package com.firelauncher.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import com.firelauncher.logging.DebugTag;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class DiagnosticUtil {

    private final static String LINE_SEPARATOR = "\n";

    public static StringBuilder deviceInformation(Activity context) {

        StringBuilder report = new StringBuilder();

        report.append("************ DEVICE INFORMATION ***********\n");

        try {
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            Integer versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;

            report.append("FireLauncher version name: ");
            report.append(versionName);
            report.append(LINE_SEPARATOR);

            report.append("FireLauncher version code: ");
            report.append(versionCode);
            report.append(LINE_SEPARATOR);


        } catch (PackageManager.NameNotFoundException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        }

        report.append("Brand: ");
        report.append(Build.BRAND);
        report.append(LINE_SEPARATOR);
        report.append("Device: ");
        report.append(Build.DEVICE);
        report.append(LINE_SEPARATOR);
        report.append("Model: ");
        report.append(Build.MODEL);
        report.append(LINE_SEPARATOR);
        report.append("Id: ");
        report.append(Build.ID);
        report.append(LINE_SEPARATOR);
        report.append("Product: ");
        report.append(Build.PRODUCT);
        report.append(LINE_SEPARATOR);
        report.append("\n************ FIRMWARE ************\n");
        report.append("SDK: ");
        report.append(Build.VERSION.SDK);
        report.append(LINE_SEPARATOR);
        report.append("Release: ");
        report.append(Build.VERSION.RELEASE);
        report.append(LINE_SEPARATOR);
        report.append("Incremental: ");
        report.append(Build.VERSION.INCREMENTAL);
        report.append(LINE_SEPARATOR);
        report.append(LINE_SEPARATOR);

        return report;
    }

    public static String getWifiSSID(Context context, String defValue) {
        try {
            WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            return wifiInfo.getSSID();
        } catch (Exception ex) {
            return defValue;
        }
    }

    public static String getActiveIpAddress(Context c, String defValue) {
        try {
            String retVal = "";
            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            if (!retVal.equals("")) {
                                retVal += ", ";
                            }
                            retVal += inetAddress.getHostAddress().toString();
                        }
                    }
                }
            } catch (SocketException ex) {
            }

            if (retVal == null || retVal.equals("")) {
                retVal = defValue;
            }

            ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            String subType = activeNetworkInfo.getSubtypeName();
            if (subType != null && !subType.equals("")) {
                subType = "-" + subType;
            }

            retVal = activeNetworkInfo.getTypeName() + subType + ": " + retVal;
            return retVal;
        } catch (Exception e) {
            return defValue;
        }
    }

}
