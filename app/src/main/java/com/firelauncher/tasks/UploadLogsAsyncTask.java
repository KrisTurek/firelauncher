package com.firelauncher.tasks;

import android.content.Context;

import com.firelauncher.R;
import com.firelauncher.tasks.commons.BaseAsyncTask;
import com.firelauncher.utils.DialogUtil;
import com.firelauncher.utils.DropboxUtil;

public class UploadLogsAsyncTask extends BaseAsyncTask<String, Void> {

    public UploadLogsAsyncTask(Context context) {
        super(context);
    }

    @Override
    public void onException(Exception e) {
        DialogUtil.toast(getContext(), e.getMessage());
    }

    @Override
    public Void doJob(String... params) {
        DropboxUtil dropbox = new DropboxUtil(getContext());
        dropbox.uploadLogs(params[0]);
        return null;
    }

    @Override
    public void onResult(Void result) {
        DialogUtil.toast(getContext(), R.string.msg_2);
    }
}
