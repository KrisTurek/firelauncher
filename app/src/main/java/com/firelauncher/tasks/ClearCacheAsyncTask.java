package com.firelauncher.tasks;

import android.content.Context;

import com.firelauncher.R;
import com.firelauncher.tasks.commons.BaseAsyncTask;
import com.firelauncher.utils.CacheUtil;
import com.firelauncher.utils.DialogUtil;

public class ClearCacheAsyncTask extends BaseAsyncTask<Void, Void> {

    public ClearCacheAsyncTask(Context context) {
        super(context);
    }

    @Override
    public void onException(Exception e) {
        DialogUtil.toast(getContext(), e.getMessage());
    }

    @Override
    public Void doJob(Void... params) {
        CacheUtil.deleteCache(getContext());
        return null;
    }

    @Override
    public void onResult(Void result) {
        DialogUtil.toast(getContext(), R.string.msg_1);
    }
}
