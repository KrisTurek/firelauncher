package com.firelauncher.tasks.commons;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;

public abstract class BaseAsyncTask<P, R> extends AsyncTask<P, Void, AsyncTaskResult<R>> {

    private Context context;
    private ICallback<R> callback;

    public BaseAsyncTask(Context context, ICallback<R> callback) {
        this.context = context;
        this.callback = callback;
    }

    public BaseAsyncTask(Context context) {
        this.context = context;
    }

    protected Context getContext() {
        return context;
    }

    protected ICallback<R> getCallback() {
        return callback;
    }

    @Override
    protected AsyncTaskResult<R> doInBackground(P... params) {
        try {
            return new AsyncTaskResult<R>(doJob(params));
        } catch (Exception e) {
            return new AsyncTaskResult<R>(e);
        }
    }

    @Override
    protected void onPostExecute(final AsyncTaskResult<R> result) {
        if (result.getError() != null) {

            Handler mainHandler = new Handler(context.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    onException(result.getError());
                }
            };
            mainHandler.post(myRunnable);

            if (DebugConfig.DEBUG)
                Log.d(DebugTag.APP, result.getError().getMessage(), result.getError());
        } else if (isCancelled()) {
            onCancel();
        } else {
            onResult(result.getResult());
        }
    }

    public void onCancel() {
    }

    public abstract void onException(Exception e);

    public abstract R doJob(P... params) throws Exception;

    public abstract void onResult(R result);

}
