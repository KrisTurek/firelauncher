package com.firelauncher.tasks.commons;

public interface ICallback<T> {

    void postExecuteCallback(T param);
}
