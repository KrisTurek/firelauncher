package com.firelauncher.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.ResultReceiver;
import android.util.Log;

import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.utils.DropboxUtil;
import com.firelauncher.utils.PrefUtil;

import java.io.File;

public class UpdateService extends IntentService {

    private DropboxUtil dropbox;

    public UpdateService() {
        super("UpdateService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateService.onCreate()");

        dropbox = new DropboxUtil(this.getApplicationContext());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (DebugConfig.DEBUG)
            Log.d(DebugTag.APP, "UpdateService.onHandleIntent()");

        ResultReceiver rec = intent.getParcelableExtra("CustomResultReceiver");

        File apk = dropbox.downloadUpdate();

        if (apk != null) {
            PrefUtil.saveToPrefs(getApplicationContext(), PrefUtil.PREFS_UPDATE_PATH_KEY, apk.getAbsolutePath());
            PrefUtil.saveToPrefs(getApplicationContext(), PrefUtil.PREFS_UPDATE_VER_KEY, dropbox.getVersion(apk.getName()).get());

            if (rec != null)
                rec.send(1, null);
        }
    }
}
