package com.firelauncher.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.firelauncher.logging.DebugTag;
import com.firelauncher.utils.PrefUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

public class SettingsProvider {

    final static String PACKAGEORDER = "packageorder_";
    private static Semaphore mSemaphore = new Semaphore(1);
    private SharedPreferences mPreferences;

    Boolean mIsLoaded = false;
    List<String> mPackageOrder = new ArrayList<>();
    Set<String> mHiddenAppsList = new HashSet<>();
    Boolean mShowSystemApps = false;

    private static SettingsProvider _instance;

    public static synchronized SettingsProvider getInstance(Context context) {
        if (SettingsProvider._instance == null) {
            SettingsProvider._instance = new SettingsProvider(context);
        }
        return SettingsProvider._instance;
    }

    private SettingsProvider(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setPackageOrder(List<String> packageOrder) {
        mPackageOrder = packageOrder;
        storeValues();
    }

    public List<String> getPackageOrder() {
        readValues();
        return mPackageOrder;
    }

    public Set<String> getHiddenApps() {
        readValues();
        return mHiddenAppsList;
    }

    public Boolean getShowSystemApps() {
        readValues();
        return mShowSystemApps;
    }

    public void readValues() {
        readValues(false);
    }

    public void readValues(Boolean forceRead) {
        try {
            // ATTENTION: NEVER CALL ONE OF THE GETTERS OR SETTERS IN HERE!
            // Aquire semaphore
            mSemaphore.acquire();

            if (mIsLoaded && !forceRead) {
                mSemaphore.release();
                return;
            }

            List<String> packageList = new ArrayList<String>();
            Integer size = mPreferences.getInt(PACKAGEORDER + "size", 0);
            for (Integer i = 0; i < size; i++) {
                String actKey = PACKAGEORDER + i.toString();
                packageList.add(mPreferences.getString(actKey, null));
            }
            mPackageOrder = packageList;

            mHiddenAppsList = mPreferences.getStringSet(PrefUtil.PREFS_HIDDEN_APPS_KEY, mHiddenAppsList);
            mShowSystemApps = mPreferences.getBoolean(PrefUtil.PREFS_SHOW_SYS_APPS_KEY, mShowSystemApps);

            mIsLoaded = true;
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }

        mSemaphore.release();
    }

    public void storeValues() {
        try {
            // ATTENTION: NEVER CALL ONE OF THE GETTERS OR SETTERS IN HERE!
            // Aquire semaphore
            mSemaphore.acquire();

            SharedPreferences.Editor editor = mPreferences.edit();
            editor.clear();

            editor.putInt(PACKAGEORDER + "size", mPackageOrder.size());
            for (Integer i = 0; i < mPackageOrder.size(); i++) {
                String actKey = PACKAGEORDER + i.toString();
                editor.remove(actKey);
                editor.putString(actKey, mPackageOrder.get(i));
            }

            editor.putStringSet(PrefUtil.PREFS_HIDDEN_APPS_KEY, mHiddenAppsList);
            editor.putBoolean(PrefUtil.PREFS_SHOW_SYS_APPS_KEY, mShowSystemApps);

            editor.commit();
        } catch (Exception e) {
            Log.d(DebugTag.APP, e.getMessage(), e.getCause());
        }

        mSemaphore.release();
    }
}
