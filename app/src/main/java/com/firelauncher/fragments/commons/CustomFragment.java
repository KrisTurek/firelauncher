package com.firelauncher.fragments.commons;

import android.app.Fragment;
import android.view.KeyEvent;

public class CustomFragment extends Fragment {

    public boolean onBackPressed() {
        return false;
    }

    public boolean onKeyDown(int keycode, KeyEvent e) {
        return false;
    }
}
