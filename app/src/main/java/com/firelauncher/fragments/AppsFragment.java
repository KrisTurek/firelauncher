package com.firelauncher.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.firelauncher.R;
import com.firelauncher.adapters.InstalledAppsAdapter;
import com.firelauncher.fragments.commons.CustomFragment;
import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.utils.AppInfo;
import com.firelauncher.utils.LauncherUtil;

import static com.firelauncher.fragments.AppSettingsDialog.newInstance;


public class AppsFragment extends CustomFragment {

    private AppInfo mMovingApp = null;
    private GridView mGridView;
    private Boolean mHasBeenInOnPauseButNotInDestroy = false;

    public AppsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.apps_fragment, container, false);

        mHasBeenInOnPauseButNotInDestroy = false;

        mGridView = (GridView) rootView.findViewById(R.id.gridview);
        mGridView.setAdapter(new InstalledAppsAdapter(getActivity()));

        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                try {
                    mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    if (mGridView.getChildCount() > 0) {
                        mGridView.requestFocusFromTouch();
                        mGridView.setSelection(0);
                    }
                } catch (Exception e) {
                    Log.e(DebugTag.APP, e.getMessage(), e.getCause());
                }
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (mMovingApp != null) {
                    // Stop moving
                    mMovingApp = null;
                    mGridView.setDrawSelectorOnTop(false);
                    mGridView.invalidate();

                    InstalledAppsAdapter actAdapter = (InstalledAppsAdapter) parent.getAdapter();
                    actAdapter.storeNewPackageOrder();
                } else {
                    String packageName = ((AppInfo) parent.getAdapter().getItem(position)).packageName;

                    LauncherUtil.startAppByPackageName(getActivity(), packageName, false, false, false);
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mMovingApp = (AppInfo) parent.getAdapter().getItem(position);
                mGridView.setDrawSelectorOnTop(true);
                mGridView.invalidate();

                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.move_app_and_click_to_drop), Toast.LENGTH_SHORT).show();

                return true;
            }
        });

        mGridView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mMovingApp != null) {
                    InstalledAppsAdapter actAdapter = (InstalledAppsAdapter) parent.getAdapter();
                    actAdapter.moveAppToPosition(mMovingApp, position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHasBeenInOnPauseButNotInDestroy = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHasBeenInOnPauseButNotInDestroy) {
            if (DebugConfig.DEBUG)
                Log.d(DebugTag.APP, "Reloading Order.");

            InstalledAppsAdapter actAdapter = (InstalledAppsAdapter) mGridView.getAdapter();
            actAdapter.loadInstalledApps();
            actAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onBackPressed() {
        boolean retVal = false;

        if (mMovingApp != null) {
            mMovingApp = null;
            mGridView.setDrawSelectorOnTop(false);
            mGridView.invalidate();

            InstalledAppsAdapter actAdapter = (InstalledAppsAdapter) mGridView.getAdapter();
            actAdapter.loadInstalledApps();
            actAdapter.notifyDataSetChanged();

            retVal = true;
        }

        return retVal;
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent e) {
        if (mMovingApp == null && keycode == KeyEvent.KEYCODE_MENU) {
            showAppSettingsDialogForCurrentApp();
            return true;
        }

        return false;
    }

    private void showAppSettingsDialogForCurrentApp() {
        try {
            if (mGridView.hasFocus()) {
                final AppInfo appInfo = (AppInfo) ((InstalledAppsAdapter) mGridView.getAdapter()).getItem(mGridView.getSelectedItemPosition());
                final AppSettingsDialog appSettingsDialog = newInstance(appInfo);

                appSettingsDialog.setOnActionClickedHandler(new AppSettingsDialog.OnActionClickedHandler() {
                    @Override
                    public void onActionClicked(AppSettingsDialog.ActionEnum action) {
                        if (DebugConfig.DEBUG)
                            Log.d(DebugTag.APP, "Clicked action: " + action.toString());

                        appSettingsDialog.dismiss();
                        switch (action) {
                            case SORT:
                                mMovingApp = appInfo;
                                mGridView.setDrawSelectorOnTop(true);
                                mGridView.invalidate();

                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.move_app_and_click_to_drop_when_started_by_menu), Toast.LENGTH_SHORT).show();
                                break;
                            case SETTINGS:
                                LauncherUtil.startSettingsViewByPackageName(AppsFragment.this.getActivity(), appInfo.packageName);
                                break;
                            default:
                                // Do nothing by default
                        }
                    }
                });

                FragmentManager fm = getActivity().getFragmentManager();
                appSettingsDialog.show(fm, "");
            }
        } catch (Exception e) {
            Log.e(DebugTag.APP, e.getMessage(), e.getCause());
        }
    }
}