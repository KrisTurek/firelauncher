package com.firelauncher.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firelauncher.R;

import com.firelauncher.utils.AppInfo;

public class AppSettingsDialog extends DialogFragment {

    private AppInfo mAppInfo;
    private OnActionClickedHandler onActionClickedHandler;

    public enum ActionEnum {NOTHING, SORT, SETTINGS}

    public static AppSettingsDialog newInstance(AppInfo appInfo) {
        AppSettingsDialog dialog = new AppSettingsDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.TransparentDialog);

        dialog.setAppInfo(appInfo);

        return dialog;
    }

    public AppSettingsDialog() {
    }

    public void setOnActionClickedHandler(OnActionClickedHandler listener) {
        onActionClickedHandler = listener;
    }

    public void setAppInfo(AppInfo appInfo) {
        mAppInfo = appInfo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.app_settings_dialog, container);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(mAppInfo.getDisplayName());

        ImageView appIcon = (ImageView) view.findViewById(R.id.appIcon);
        appIcon.setImageDrawable(mAppInfo.getDisplayIcon());

        LinearLayout appSort = (LinearLayout) view.findViewById(R.id.appSort);
        appSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fireActionClicked(ActionEnum.SORT);
            }
        });

        LinearLayout appSettings = (LinearLayout) view.findViewById(R.id.appSettings);
        appSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fireActionClicked(ActionEnum.SETTINGS);
            }
        });

        return view;
    }

    private void fireActionClicked(ActionEnum action) {
        if (onActionClickedHandler != null) {
            onActionClickedHandler.onActionClicked(action);
        }
    }

    public interface OnActionClickedHandler {
        void onActionClicked(ActionEnum action);
    }
}