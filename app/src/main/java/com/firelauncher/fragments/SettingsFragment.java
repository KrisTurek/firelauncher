package com.firelauncher.fragments;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.firelauncher.R;
import com.firelauncher.adapters.InstalledAppsAdapter;
import com.firelauncher.logging.DebugConfig;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.tasks.ClearCacheAsyncTask;
import com.firelauncher.tasks.UploadLogsAsyncTask;
import com.firelauncher.utils.AppInfo;
import com.firelauncher.utils.DiagnosticUtil;
import com.firelauncher.utils.PrefUtil;

import java.util.List;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

        try {
            Preference softwarePref = findPreference(PrefUtil.PREFS_SOFTWARE_KEY);
            softwarePref.setSummary(getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName);

            Preference wifiNamePref = findPreference(PrefUtil.PREFS_WIFI_NAME_KEY);
            wifiNamePref.setSummary(DiagnosticUtil.getWifiSSID(this.getActivity(), getActivity().getResources().getString(R.string.not_found_summary_settings)));

            Preference ipAddressPref = findPreference(PrefUtil.PREFS_IP_ADDRESS_KEY);
            ipAddressPref.setSummary(DiagnosticUtil.getActiveIpAddress(getActivity(), getActivity().getResources().getString(R.string.not_found_summary_settings)));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(DebugTag.APP, e.getMessage(), e);
        }

        Preference clearCachePref = findPreference(PrefUtil.PREFS_CLEAR_CACHE_KEY);
        clearCachePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ClearCacheAsyncTask task = new ClearCacheAsyncTask(getActivity().getApplicationContext());
                task.execute();

                return true;
            }
        });

        Preference uploadLogsPref = findPreference(PrefUtil.PREFS_UPLOAD_LOGS_KEY);
        uploadLogsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                UploadLogsAsyncTask task = new UploadLogsAsyncTask(getActivity().getApplicationContext());
                task.execute(DiagnosticUtil.deviceInformation(getActivity()).toString());

                return true;
            }
        });

        InstalledAppsAdapter actAppsAdapter = new InstalledAppsAdapter(getActivity(), true, false);
        List<AppInfo> actApps = actAppsAdapter.getAppList();

        CharSequence[] entries = new CharSequence[actApps.size() + 1];
        CharSequence[] entryValues = new CharSequence[actApps.size() + 1];

        entries[0] = " - No Action - ";
        entryValues[0] = "";

        for (Integer i = 1; i < actApps.size() + 1; i++) {
            AppInfo actApp = actApps.get(i - 1);
            entries[i] = actApp.getDisplayName();
            entryValues[i] = actApp.packageName;
        }

        InstalledAppsAdapter actHiddenAppsAdapter = new InstalledAppsAdapter(getActivity(), true, true);
        List<AppInfo> actHiddenApps = actHiddenAppsAdapter.getAppList();
        CharSequence[] hiddenEntries = new CharSequence[actHiddenApps.size()];
        CharSequence[] hiddenEntryValues = new CharSequence[actHiddenApps.size()];
        for (Integer i = 0; i < actHiddenApps.size(); i++) {
            AppInfo actApp = actHiddenApps.get(i);
            hiddenEntries[i] = actApp.getDisplayName();
            hiddenEntryValues[i] = actApp.packageName;
        }

        MultiSelectListPreference hiddenAppsPref = (MultiSelectListPreference) findPreference(PrefUtil.PREFS_HIDDEN_APPS_KEY);
        hiddenAppsPref.setEntries(hiddenEntries);
        hiddenAppsPref.setEntryValues(hiddenEntryValues);
        hiddenAppsPref.setDefaultValue(mSettings.getHiddenApps());
    }

    @Override
    public void onResume() {
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(mSharedPreferenceListener);
        super.onResume();
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(mSharedPreferenceListener);

        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PrefUtil.PREFS_DEBUG_KEY:
                DebugConfig.DEBUG = sharedPreferences.getBoolean(key, PrefUtil.DEFAULT_DEBUG);
                break;
            default:
                break;
        }
    }

    SettingsProvider mSettings = SettingsProvider.getInstance(this.getActivity());

    SharedPreferences.OnSharedPreferenceChangeListener mSharedPreferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Thread readValuesThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mSettings.readValues(true);
                }
            });
            readValuesThread.start();
        }
    };
}
