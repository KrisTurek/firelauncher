package com.firelauncher.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.firelauncher.R;
import com.firelauncher.fragments.AppsFragment;
import com.firelauncher.fragments.SettingsFragment;

import java.util.ArrayList;
import java.util.List;


public class MenuItemsListAdapter extends BaseAdapter {

    private Context mContext;
    private static List<FragmentListItem> mItems = null;

    public static class FragmentListItem {
        public String description;
        public String className;


        public FragmentListItem(String description, String className) {
            this.description = description;
            this.className = className;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    public MenuItemsListAdapter(Context context) {
        mContext = context;
        if (mItems == null) {
            mItems = new ArrayList<FragmentListItem>();
            mItems.add(new FragmentListItem(mContext.getResources().getString(R.string.menu_all_apps), AppsFragment.class.getName()));
            mItems.add(new FragmentListItem(mContext.getResources().getString(R.string.menu_settings), SettingsFragment.class.getName()));
        }
    }

    public int getCount() {
        return mItems.size();
    }

    public FragmentListItem getItem(int position) {
        return mItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView;

        if (convertView == null)
            itemView = inflater.inflate(R.layout.menu_item_layout, parent, false);
        else
            itemView = (View) convertView;

        TextView textView = (TextView) itemView.findViewById(R.id.menu_display_text);
        textView.setText(mItems.get(position).description);

        return itemView;
    }
}
