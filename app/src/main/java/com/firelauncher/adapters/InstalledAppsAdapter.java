package com.firelauncher.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firelauncher.R;
import com.firelauncher.fragments.SettingsProvider;
import com.firelauncher.logging.DebugTag;
import com.firelauncher.utils.AppInfo;
import com.firelauncher.utils.LauncherUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InstalledAppsAdapter extends BaseAdapter {

    public static final String VIRTUAL_SETTINGS_PACKAGE = "com.firelauncher.virtual.settings";

    private Context mContext;
    private List<AppInfo> mInstalledApps;
    private String mDefaultLauncherPackage;
    private SettingsProvider mSettings;
    private Boolean mIncludeOwnApp;
    private Boolean mShowHiddenApps;

    public InstalledAppsAdapter(Context c) {
        this(c, false, false);
    }

    public InstalledAppsAdapter(Context c, Boolean includeOwnApp, Boolean showHiddenApps) {
        mContext = c;
        mIncludeOwnApp = includeOwnApp;
        mShowHiddenApps = showHiddenApps;
        mDefaultLauncherPackage = LauncherUtil.getLauncherPackageName(mContext);
        mSettings = SettingsProvider.getInstance(mContext);

        loadInstalledApps();
    }

    @SuppressLint("NewApi")
    public static Intent getLaunchableIntentByPackageName(Context context, String packageName) {
        Intent launchIntent = null;

        if (packageName.equals(VIRTUAL_SETTINGS_PACKAGE)) {
            launchIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);

            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            try {
                launchIntent = context.getPackageManager().getLeanbackLaunchIntentForPackage(packageName);
            } catch (Throwable ignore) {
            }

            if (launchIntent == null) {
                launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            }
        }

        return launchIntent;
    }

    public int getCount() {
        return mInstalledApps.size();
    }

    public Object getItem(int position) {
        return mInstalledApps.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public List<AppInfo> getAppList() {
        return mInstalledApps;
    }

    public void storeNewPackageOrder() {
        List<String> packageList = new ArrayList<>();
        for (AppInfo actApp : mInstalledApps) {
            packageList.add(actApp.packageName);
        }
        mSettings.setPackageOrder(packageList);
    }

    public void moveAppToPosition(AppInfo app, int position) {
        if (mInstalledApps.contains(app)) {
            try {
                mInstalledApps.remove(app);
                mInstalledApps.add(position, app);
                notifyDataSetChanged();
            } catch (Exception e) {
                Log.d(DebugTag.APP, e.getMessage(), e.getCause());
            }
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        AppInfo actApp = mInstalledApps.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        if (convertView == null) {
            gridView = inflater.inflate(R.layout.apps_grid_item_layout, parent, false);
        } else {
            gridView = convertView;
        }

        TextView textView = (TextView) gridView.findViewById(R.id.textLabel);
        textView.setText(actApp.getDisplayName());

        ImageView imageView = (ImageView) gridView.findViewById(R.id.imageLabel);
        imageView.setImageDrawable(actApp.getDisplayIcon());

        return gridView;
    }

    public void loadInstalledApps() {
        Set<String> hiddenApps;
        if (mShowHiddenApps) {
            hiddenApps = new HashSet<>();
        } else {
            hiddenApps = mSettings.getHiddenApps();
        }

        PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> installedApplications = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        Boolean includeSysApps = mSettings.getShowSystemApps();
        String ownPackageName = mContext.getApplicationContext().getPackageName();
        Map<String, ApplicationInfo> appMap = new LinkedHashMap<String, ApplicationInfo>();
        for (ApplicationInfo installedApplication : installedApplications) {
            if (!hiddenApps.contains(installedApplication.packageName)) {
                Boolean isSystemApp = ((installedApplication.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 1 ||
                        (installedApplication.flags & ApplicationInfo.FLAG_SYSTEM) == 1);

                if (isSystemApp && installedApplication.packageName.equals(mDefaultLauncherPackage)) {
                    isSystemApp = false;
                }

                if (includeSysApps || !isSystemApp) {
                    if ((mIncludeOwnApp || !installedApplication.packageName.equals(ownPackageName))) {
                        appMap.put(installedApplication.packageName, installedApplication);
                    }
                }
            }
        }

        if (!hiddenApps.contains(VIRTUAL_SETTINGS_PACKAGE)) {
            ApplicationInfo applicationInfo = new ApplicationInfo();
            applicationInfo.packageName = VIRTUAL_SETTINGS_PACKAGE;
            appMap.put(VIRTUAL_SETTINGS_PACKAGE, new AppInfo(mContext, applicationInfo) {
                @Override
                public String getDisplayName() {
                    return mContext.getResources().getString(R.string.amazon_settings);
                }

                @Override
                public Drawable getDisplayIcon() {
                    Drawable retVal = null;

                    try {
                        retVal = new BitmapDrawable(mContext.getResources(), BitmapFactory.decodeStream(mContext.getAssets().open("firetv-settings-icon.png")));
                    } catch (Exception ignore) {
                    }

                    return retVal;
                }
            });
        }

        List<String> packageOrder = mSettings.getPackageOrder();

        mInstalledApps = new ArrayList<AppInfo>();

        for (String packageName : packageOrder) {
            if (appMap.containsKey(packageName)) {
                addAppToCurrentList(appMap.get(packageName));
                appMap.remove(packageName);
            }
        }

        if (appMap.containsKey(mDefaultLauncherPackage)) {
            addAppToCurrentList(appMap.get(mDefaultLauncherPackage));
            appMap.remove(mDefaultLauncherPackage);
        }

        if (appMap.containsKey(VIRTUAL_SETTINGS_PACKAGE)) {
            addAppToCurrentList(appMap.get(VIRTUAL_SETTINGS_PACKAGE));
            appMap.remove(VIRTUAL_SETTINGS_PACKAGE);
        }

        for (ApplicationInfo installedApplication : appMap.values()) {
            addAppToCurrentList(installedApplication);
        }
    }

    private void addAppToCurrentList(ApplicationInfo app) {
        if (app.packageName.equals(mDefaultLauncherPackage)) {
            AppInfo amazonLauncher = new AppInfo(mContext, app) {
                @Override
                public String getDisplayName() {
                    return mContext.getResources().getString(R.string.amazon_home);
                }

                @Override
                public Drawable getDisplayIcon() {
                    Drawable retVal = null;

                    try {
                        retVal = new BitmapDrawable(mContext.getResources(), BitmapFactory.decodeStream(mContext.getAssets().open("firetv-home-icon.png")));
                    } catch (Exception ignore) {
                    }

                    return retVal;
                }
            };

            mInstalledApps.add(amazonLauncher);
        } else {
            if (app instanceof AppInfo) {
                mInstalledApps.add((AppInfo) app);
            } else {
                mInstalledApps.add(new AppInfo(mContext, app));
            }
        }
    }
}
