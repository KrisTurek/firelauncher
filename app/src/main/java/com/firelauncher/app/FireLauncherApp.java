package com.firelauncher.app;

import android.app.Application;

import com.firelauncher.logging.DebugConfig;
import com.firelauncher.utils.PrefUtil;

public class FireLauncherApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DebugConfig.DEBUG = PrefUtil.getFromPrefs(this, PrefUtil.PREFS_DEBUG_KEY, PrefUtil.DEFAULT_DEBUG);
    }
}
